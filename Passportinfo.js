import React from 'react';
import {
    SafeAreaView,

    StatusBar,
    StyleSheet,
    Text,
    ImageBackground,
    useColorScheme,
    View,
    TouchableOpacity,
    Button,
    TextInput,
    ScrollView,
} from 'react-native';






const Passportinfo = () => {


    return (





        <ImageBackground style={styles.container} source={require('./images/Background.png')}>


            <View style={styles.headercontainer}><Text>Rahil</Text>
            </View>
            <View style={styles.Txtcontainer}><Text style={styles.txtstyl}>Passport Information</Text></View>
            <View style={styles.inptxt}><Text style={styles.headertxt}>Full name*</Text></View>
            <View style={styles.inpcontainer}><TextInput placeholder='Name' /></View>

            <View style={styles.inptxt}><Text style={styles.headertxt}>Passport Number*</Text></View>
            <View style={styles.inpcontainer}><TextInput placeholder='enter passport number' /></View>

            <View style={styles.inptxt}><Text style={styles.headertxt}>Nationality*</Text></View>
            <View style={styles.inpcontainer}><TextInput placeholder='Enter the Nationality' /></View>
            <View style={[styles.inpcontainer, { backgroundColor: "#0a0e45", alignItems: 'center', marginTop: "7%" }]}><TouchableOpacity><Text style={styles.btntxt}>Continue</Text></TouchableOpacity></View>


            <View style={styles.lsttxt}><TouchableOpacity><Text style={styles.lsttxtstyl}>Your data is safe with us</Text></TouchableOpacity></View>





        </ImageBackground>











    );
};



const styles = StyleSheet.create({

    container: {
        marginTop: "10%",

        flex: 1

    },

    headercontainer: {

        height: "13%"

    },
    Txtcontainer: {
        height: "10%",
        backgroundColor: 'white',
        alignItems: 'center'
    },
    txtstyl: {

        fontSize: 20,
        fontWeight: "600",
        color: '#0a0e45'
    },



    inpcontainer: {

        alignSelf: 'flex-end',
        alignItems: 'center',
        width: "85%",
        height: "6%",
        backgroundColor: '#fff',
        borderRadius: 25,
        borderColor: '#0a0e45',
        borderWidth: 1,
        marginRight: "7.5%",

        justifyContent: "center",
        marginBottom: "3%"



    },
    inptxt: {
        alignItems: 'center',
        marginBottom: "2%"

    },
    headertxt: {
        fontSize: 16,
        color: "grey"
    },
    btntxt: {
        color: '#fff',
        fontWeight: "700"
    },
    lsttxt: {
        alignItems: 'center'
    },
    lsttxtstyl: {
        textDecorationLine: "underline"
    }


});

export default Passportinfo;

