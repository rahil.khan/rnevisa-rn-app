import React from 'react';
import {
    SafeAreaView,

    StatusBar,
    StyleSheet,
    Text,
    ImageBackground,
    useColorScheme,
    View,
    TouchableOpacity,
    Button,
    TextInput,
    ScrollView,
} from 'react-native';





const Login1 = () => {


    return (


        <ImageBackground source={require('./images/Background.png')} style={styles.bgimage} resizeMode='stretch' >

            <View style={styles.loginstyl}><Text style={styles.logintxt}>Login Account</Text></View>
            <View style={styles.loginstyl2}><Text style={styles.logintxt2}>Hello, Welcome back to our account</Text></View>
            <View style={styles.inpconatiner1}><View style={styles.btn1} ><Button title='Email' /></View><View style={styles.btn2}><Button title='Phone Number' /></View></View>
            <View style={styles.inpconatiner}><TextInput placeholder='enter the email' keyboardType='email-address' style={styles.txtinp} /></View>
            <View style={styles.inpconatiner}><TextInput placeholder='enter th password' secureTextEntry style={styles.txtinp} /></View>
            <View style={styles.btnconatiner1}><TouchableOpacity ><Text style={styles.btntxt}>Request OTP</Text></TouchableOpacity></View>
            <View style={styles.txtcontainer}><TouchableOpacity><Text >--------   or signin with google   --------</Text></TouchableOpacity></View>
            <View style={styles.btnconatiner2}><TouchableOpacity><Text style={styles.btntxt}>Continue with Google</Text></TouchableOpacity></View>

        </ImageBackground >



    );
};

const styles = StyleSheet.create({
    bgimage: {
        marginTop: "8.7%",

        alignItems: 'center',
        flex: 1
    },

    txtinp: {
        marginTop: 11,
        marginLeft: 5
    },

    logintxt: {
        fontSize: 20,
        fontWeight: '600',
        color: '#0a0e45'

    },
    logintxt2: {
        fontSize: 16,
        color: 'grey'
    },
    btn1: {
        backgroundColor: '#0a0e45',//bluecolor//

        justifyContent: 'space-between',
        marginRight: 120,
        borderRadius: 15,
        width: 100,
        height: 40,
        marginTop: 5,




    },
    btn2: {
        backgroundColor: '#0a0e45',
        justifyContent: 'space-between',
        borderRadius: 15,
        width: 100,
        marginTop: 4,
        marginBottom: 4



    },
    inpconatiner: {
        width: '82%',
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 20,
        borderColor: 'grey',
        borderWidth: 1,
        marginBottom: '10%',
        alignItems: 'flex-start',




    },
    loginstyl: {
        marginTop: '6%',
        width: '90%'


    },
    loginstyl2: {

        width: '90%',
        marginBottom: '10%'


    },

    txtcontainer: {

        marginBottom: '10%',


    },
    inpconatiner1: {
        width: '82%',
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 20,
        borderColor: 'grey',
        borderWidth: 1,
        marginBottom: '10%',
        flexDirection: 'row',




    },
    btnconatiner1: {
        width: '82%',
        height: 50,
        backgroundColor: '#0a0e45',
        borderRadius: 20,
        borderColor: 'grey',
        borderWidth: 1,
        marginBottom: '10%',



    },
    btnconatiner2: {
        width: '82%',
        height: 50,
        backgroundColor: 'green',
        borderRadius: 20,
        borderColor: 'grey',
        borderWidth: 1,
        marginBottom: '10%',


    },
    btntxt: {
        color: '#fff',
        fontWeight: '700',
        alignSelf: 'center',
        marginTop: '5%'
    }

});

export default Login1;

