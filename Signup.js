

import React from 'react';

import { StyleSheet, Text, View, TextInput, TouchableOpacity, ScrollView, ImageBackground } from 'react-native';



const Signup = () => {
    return (
        <ImageBackground source={require('./images/Background.png')} style={styles.bgimage} resizeMode='stretch' >
            <ScrollView>
                <View style={styles.container}>
                    <View style={styles.signup}>
                        <Text style={styles.signuptxt}>Signup</Text>

                    </View>
                    <View style={styles.nameinput}>
                        <Text style={styles.nametxt}>Name*</Text>
                        <TextInput style={styles.txtinput} />

                    </View>
                    <View style={styles.nameinput}>
                        <Text style={styles.nametxt}>Email Id*</Text>
                        <TextInput style={styles.txtinput} keyboardType='email-address' />

                    </View>
                    <View style={styles.nameinput}>
                        <Text style={styles.nametxt}>Phone Number*</Text>
                        <TextInput style={styles.txtinput} keyboardType='phone-pad' />

                    </View>
                    <View style={styles.nameinput}>
                        <Text style={styles.nametxt}>Password*</Text>
                        <TextInput style={styles.txtinput} secureTextEntry />

                    </View>
                    <View style={styles.nameinput}>

                        <TouchableOpacity style={styles.btn}><Text style={styles.btntxt}>Request OTP</Text></TouchableOpacity>

                    </View>
                    <View style={styles.nameinput}>

                        <TouchableOpacity style={styles.btn2}><Text style={styles.btntxt2} >Signin with Google</Text></TouchableOpacity>

                    </View>

                </View>
                <View style={styles.lsttxt}><TouchableOpacity><Text style={styles.lsttxtstyl}>Your data is safe with us</Text></TouchableOpacity></View>
            </ScrollView>
        </ImageBackground>


    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 90,
        flex: 1,
        backgroundColor: '#fff',

        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    bgimage: {
        marginTop: "8.7%",

        alignItems: 'center',
        flex: 1
    },
    signup: {

        alignItems: 'flex-start',
        marginLeft: 12,


    },
    signuptxt: {
        fontSize: 20,
        fontWeight: "500",



    },
    txtinput: {
        backgroundColor: '#E8E8E8',//grey//
        width: 340,
        height: 40,
        borderRadius: 15,


    },
    nameinput: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 8

    },
    nametxt: {
        color: "grey",
        marginBottom: 4

    },
    btn: {
        backgroundColor: '#0a0e45',
        width: 340,
        height: 40,
        borderRadius: 15,
        marginTop: 15,
        justifyContent: 'center'

    },
    btn2: {
        backgroundColor: 'green',
        width: 340,
        height: 40,
        borderRadius: 15,
        marginTop: 15,
        justifyContent: 'center'

    }
    ,
    btntxt2: {
        color: 'black',
        textAlign: 'center',
        fontWeight: '500'

    },
    btntxt: {

        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'


    },
    lasttxt: {
        alignItems: 'center',
        textDecorationLine: "underline"


    }
    ,
    lastv: {

        marginTop: 5,


    },
    lsttxt: {
        alignItems: 'center',
        marginTop: "2%"
    },
    lsttxtstyl: {
        textDecorationLine: "underline",
    }




});
export default Signup;
