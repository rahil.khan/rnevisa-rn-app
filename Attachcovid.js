import React from 'react';
import {
    SafeAreaView,

    StatusBar,
    StyleSheet,
    Text,
    ImageBackground,
    useColorScheme,
    View,
    TouchableOpacity,
    Button,
    TextInput,
    ScrollView,
    Image
} from 'react-native';






const Attachcovid = () => {


    return (

        <View style={styles.container}>
            <ImageBackground source={require('./images/Background.png')} style={styles.bgimage} resizeMode='stretch' >


                <View style={styles.headercontainer}><View style={{}}><Image source={require('./images/Onboarding-icon.png')} resizeMode='stretch' /></View>
                </View>
                <View style={styles.Txtcontainer}><Text style={styles.txtstyl}>Attached your Covid Entry</Text></View>
                <View style={styles.imgstyl}><Image source={require('./images/Attachedcovid.png')} style={{ height: 220, width: 220 }} /></View>


                <View style={[styles.inpcontainer, { backgroundColor: "#0a0e45", alignItems: 'center', marginTop: "7%" }]}><TouchableOpacity><Text style={styles.btntxt}>Upload</Text></TouchableOpacity></View>


                <View style={styles.lsttxt}><TouchableOpacity><Text style={styles.lsttxtstyl}>Your data is safe with us</Text></TouchableOpacity></View>



            </ImageBackground >

        </View>













    );
};



const styles = StyleSheet.create({

    container: {
        marginTop: "10%",

        flex: 1

    },
    bgimage: {
        marginTop: "8.7%",

        alignItems: 'center',
        flex: 1
    },
    imgstyl: {
        alignItems: 'center'
    },


    headercontainer: {


        alignItems: "center"


    },
    Txtcontainer: {
        height: "10%",
        backgroundColor: 'white',
        alignItems: 'center'
    },
    txtstyl: {

        fontSize: 20,
        fontWeight: "600",
        color: '#0a0e45'     //blue/color/
    },



    inpcontainer: {

        alignSelf: 'flex-end',
        alignItems: 'center',
        width: "85%",
        height: "6%",
        backgroundColor: '#fff',
        borderRadius: 25,
        borderColor: 'blue',
        borderWidth: 1,
        marginRight: "7.5%",

        justifyContent: "center",
        marginBottom: "3%"



    },
    inptxt: {
        alignItems: 'center',
        marginBottom: "2%"

    },
    headertxt: {
        fontSize: 16,
        color: "grey"
    },
    btntxt: {
        color: '#fff',
        fontWeight: "700"
    },
    lsttxt: {
        alignItems: 'center'
    },
    lsttxtstyl: {
        textDecorationLine: "underline",
    }


});

export default Attachcovid;

